package com.woodws.mybatis.samples.dao;

import com.woodws.mybatis.jpa.CommonMapper;
import com.woodws.mybatis.samples.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface UserMapper extends CommonMapper<User, Integer> {

    Date funcSysdate();

    List<User> findOrderByAge();

    List<User> findOrderByAgeDesc();

    List<User> findByNameOrderByAge(String name);

    List<User> findByNameOrAgeGreaterOrderByAge(@Param("name") String name,
                                                @Param("age") Integer age);

    String findNameById(Integer id);

    int updateName(User user);

    List<User> findByNameLike(String name);

}